﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon.SQS.Model;

namespace Kafka.DataRecovery.Worker.Repositories
{
    public interface IAmazonRepository : IDisposable
    {
        Task<ReceiveMessageResponse> GetMessagesAsync();

        Task<DeleteMessageBatchResponse> DeleteMessagesAsync(List<Message> messages);
    }
}