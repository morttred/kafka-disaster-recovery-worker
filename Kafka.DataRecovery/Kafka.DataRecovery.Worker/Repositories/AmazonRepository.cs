﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.Runtime;
using Amazon.SQS;
using Amazon.SQS.Model;
using Kafka.DataRecovery.Worker.Configuration;

namespace Kafka.DataRecovery.Worker.Repositories
{
    public class AmazonRepository : IAmazonRepository
    {
        public AmazonRepositoryConfig Config { get; set; }

        public AmazonSQSClient AmazonSqsClient { get; set; }

        public AmazonRepository(AmazonRepositoryConfig config)
        {
            AmazonSqsClient = new AmazonSQSClient(
                new BasicAWSCredentials(config.AccessKey, config.SecretKey),
                new AmazonSQSConfig { CacheHttpClient = true, Timeout = TimeSpan.FromMinutes(2) });
        }

        public async Task<ReceiveMessageResponse> GetMessagesAsync()
        {
            var request = new ReceiveMessageRequest
            {
                MaxNumberOfMessages = Config.MaxNumberOfMessages,
                VisibilityTimeout = Config.VisibilityTimeout,
                WaitTimeSeconds = Config.WaitTimeSeconds,
                AttributeNames = Config.AttributeNames,
                QueueUrl = Config.QueueUrl
            };

            var response = await AmazonSqsClient.ReceiveMessageAsync(request);

            return response;
        }

        public async Task<DeleteMessageBatchResponse> DeleteMessagesAsync(List<Message> messages)
        {
            var entries = messages.Select(m => new DeleteMessageBatchRequestEntry(m.MessageId, m.ReceiptHandle))
                .ToList();

            var request = new DeleteMessageBatchRequest
            {
                QueueUrl = Config.QueueUrl,
                Entries = entries
            };

            var response = await AmazonSqsClient.DeleteMessageBatchAsync(request);

            return response;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                AmazonSqsClient?.Dispose();
            }
        }
    }
}
