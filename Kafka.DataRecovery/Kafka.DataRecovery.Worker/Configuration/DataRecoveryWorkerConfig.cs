﻿using StatsCollector;

namespace Kafka.DataRecovery.Worker.Configuration
{
    public class DataRecoveryWorkerConfig
    {
        public StatsCollectorConfig StatsCollectorConfig { get; set; }

        public KafkaProducerConfig KafkaProducerConfig { get; set; }

        public AmazonRepositoryConfig AmazonQueueConfig { get; set; }

        public int ParallelWorkerTasks { get; set; } = 100;

        public int MillisecondsToWaitOnEmptyQueue { get; set; } = 100;
    }
}
