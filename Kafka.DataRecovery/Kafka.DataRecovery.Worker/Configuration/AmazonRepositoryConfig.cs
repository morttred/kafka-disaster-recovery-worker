﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Kafka.DataRecovery.Worker.Configuration
{
    public class AmazonRepositoryConfig
    {
        [JsonRequired]
        public string AccessKey { get; set; }

        [JsonRequired]
        public string SecretKey { get; set; }

        [JsonRequired]
        public string QueueUrl { get; set; }

        [JsonRequired]
        public int WaitTimeSeconds { get; set; }

        [JsonRequired]
        public int VisibilityTimeout { get; set; }

        [JsonRequired]
        public int MaxNumberOfMessages { get; set; }

        [JsonRequired]
        public List<string> AttributeNames { get; set; } =
            new List<string> { "CreatedTimestamp" };

        [JsonRequired]
        public DateTime CreatedAfter { get; set; }
    }
}
