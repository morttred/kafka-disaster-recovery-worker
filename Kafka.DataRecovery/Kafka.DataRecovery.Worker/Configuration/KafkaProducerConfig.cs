﻿using Newtonsoft.Json;

namespace Kafka.DataRecovery.Worker.Configuration
{
    public class KafkaProducerConfig
    {
        public const int DefaultBatchSize = 1000;
        public const int DefaultRequiredAcks = 1;
        public const int DefaultLingerMilliseconds = 1000;
        public const string DefaultCompressionCodec = "gzip";

        /// <summary>
        /// Sets property: bootstrap.servers
        /// Description: Initial list of brokers as a CSV list of broker host or host:port. 
        /// </summary>
        [JsonRequired]
        public string BrokersList { get; set; }

        /// <summary>
        /// Sets property: compression.codec
        /// Description: Compression codec to use for compressing message sets. 
        /// This is the default value for all topics, may be overriden by the 
        /// topic configuration property compression.codec. 
        /// </summary>
        public string CompressionCodec { get; set; } = DefaultCompressionCodec;

        /// <summary>
        /// Sets property: batch.num.messages
        /// Description: Maximum number of messages batched in one MessageSet.
        /// </summary>
        public int BatchSize { get; set; } = DefaultBatchSize;

        /// <summary>
        /// Sets property: request.required.acks
        /// Description: This field indicates how many acknowledgements the leader 
        /// broker must receive from ISR brokers before responding to the request: 
        /// 0 = Broker does not send any response/ack to client, 
        /// 1 = Only the leader broker will need to ack the message, 
        /// -1 or all = broker will block until message is committed by all in sync 
        /// replicas (ISRs) or broker's in.sync.replicas setting before sending response. 
        /// </summary>
        public int RequiredAcks { get; set; } = DefaultRequiredAcks;

        /// <summary>
        /// Sets property: queue.buffering.max.ms
        /// Description: Delay in milliseconds to wait for messages in the producer queue 
        /// to accumulate before constructing message batches (MessageSets) to transmit to 
        /// brokers. A higher value allows larger and more effective (less overhead, improved compression) 
        /// batches of messages to accumulate at the expense of increased message delivery latency. 
        /// </summary>
        public int LingerMilliseconds { get; set; } = DefaultLingerMilliseconds;
    }
}