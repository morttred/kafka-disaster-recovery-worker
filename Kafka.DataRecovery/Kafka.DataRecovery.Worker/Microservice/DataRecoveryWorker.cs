﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.SQS.Model;
using Confluent.Kafka;
using Kafka.DataRecovery.Worker.Configuration;
using Kafka.DataRecovery.Worker.Extensions;
using Kafka.DataRecovery.Worker.Repositories;
using Serilog;
using StatsCollector;
using Message = Amazon.SQS.Model.Message;

namespace Kafka.DataRecovery.Worker.Microservice
{
    public class DataRecoveryWorker : IDisposable
    {
        public const string MqttTopic = nameof(MqttTopic);

        public DataRecoveryWorker(ILogger logger,
            IStatsCollector statsCollector,
            Producer<string, string> kafkaProducer,
            IAmazonRepository amazonRepository)
        {
            Logger = logger;
            StatsCollector = statsCollector;
            KafkaProducer = kafkaProducer;
            AmazonRepository = amazonRepository;
            WorkerTasksQueue = new ConcurrentQueue<Action>();
        }

        protected ILogger Logger { get; set; }

        protected IStatsCollector StatsCollector { get; set; }

        protected IAmazonRepository AmazonRepository { get; }

        protected Producer<string, string> KafkaProducer { get; set; }

        protected ConcurrentQueue<Action> WorkerTasksQueue { get; set; }

        protected DataRecoveryWorkerConfig WorkerConfiguration { get; set; }

        public void Initialize()
        {
            InitializeWorkerTasksQueue();
        }

        public async Task Start()
        {
            while (true)
            {
                if (WorkerTasksQueue.IsEmpty)
                {
                    await Task.Delay(WorkerConfiguration.MillisecondsToWaitOnEmptyQueue);
                }

                WorkerTasksQueue.TryDequeue(out var workerTask);
                workerTask?.Invoke();
            }
        }

        private void InitializeWorkerTasksQueue()
        {
            WorkerConfiguration.ParallelWorkerTasks.ForEach(
                () => WorkerTasksQueue.Enqueue(
                    () => WorkerTask()));
        }

        public async Task WorkerTask()
        {
            var messages = await GetMessagesFromAmazonQueue();
            if (messages.Any())
            {
                var publishReports = await PublishMessagesToKafka(messages);
                if (publishReports.All(IsSuccessful))
                {
                    await DeleteMessages(messages);
                }

                WorkerTasksQueue.Enqueue(async () => await WorkerTask());
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                AmazonRepository?.Dispose();
                KafkaProducer?.Dispose();
            }
        }

        private bool HasMqttTopicAttribute(Message message)
        {
            return message.Attributes.ContainsKey(MqttTopic);
        }

        private bool IsSuccessful(Message<string, string> report)
        {
            return !report.Error.HasError;
        }

        private async Task DeleteMessages(List<Message> messages)
        {
            var deleteMessagesResponse = await DeleteMessagesFromAmazonQueue(messages);

            var deletedMessagesCount = deleteMessagesResponse.Successful.Count;
            if (deletedMessagesCount == messages.Count)
            {
                Logger.Information("Deleted messages: {DeletedMessages}", deletedMessagesCount);
                StatsCollector.Counter("DeletedMessages", deletedMessagesCount);
            }
            else
            {
                Logger.Error("Not all messages from the batch were successfully deleted.");
                Logger.Error("Response: {@DeleteMessagesResponse}", deleteMessagesResponse);
            }
        }

        private async Task<List<Message>> GetMessagesFromAmazonQueue()
        {
            var getMessagesResponse = await StatsCollector.Time(
                async () => await AmazonRepository.GetMessagesAsync(),
                "GetMessagesAsyncTime");

            var messages = getMessagesResponse.Messages;
            if (!messages.Any())
            {
                Logger.Information("No messages received. GetMessagesResponse: {@Response}.", getMessagesResponse);
            }

            return messages;
        }

        private async Task<DeleteMessageBatchResponse> DeleteMessagesFromAmazonQueue(List<Message> messages)
        {
            var deleteMessagesResponse = await StatsCollector.Time(
                async () => await AmazonRepository.DeleteMessagesAsync(messages),
                "DeleteMessagesAsyncTime");

            return deleteMessagesResponse;
        }

        private async Task<Message<string, string>> PublishMessageToKafka(Message message)
        {
            var kafkaTopic = message.Attributes[MqttTopic].Replace('/', '.');
            var kafkaMessageKey = message.MessageId;
            var kafkaMessageValue = message.Body;

            var producerReport = await StatsCollector.Time(
                async () => await KafkaProducer.ProduceAsync(kafkaTopic, kafkaMessageKey, kafkaMessageValue),
                "ProduceAsyncTime");

            if (producerReport.Error.HasError)
            {
                Logger.Error("Error while trying to publish a message to Kafka: {@Error}.", producerReport.Error);
                StatsCollector.Counter("Errors", 1);
            }
            else
            {
                Logger.Information("Produced a message to topic: {Topic}", producerReport.Topic);
                StatsCollector.Counter("ProduceAsync", 1);
            }

            return producerReport;
        }
       
        private Task<IEnumerable<Message<string, string>>> PublishMessagesToKafka(IEnumerable<Message> messages)
        {
            return messages.Where(HasMqttTopicAttribute)
                .Select(PublishMessageToKafka)
                .WhenAll();
        }
    }
}
