﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kafka.DataRecovery.Worker.Extensions
{
    public static class EnumerableExtensions
    {
        public static async Task<IEnumerable<TResult>> WhenAll<TResult>(this IEnumerable<Task<TResult>> tasks)
        {
            return await Task.WhenAll(tasks);
        }
    }
}
