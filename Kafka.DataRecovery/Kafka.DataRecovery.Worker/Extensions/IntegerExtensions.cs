﻿using System;

namespace Kafka.DataRecovery.Worker.Extensions
{
    public static class IntegerExtensions
    {
        public static void ForEach(this int number, Action action)
        {
            for (var i = 0; i < number; i++)
            {
                action.Invoke();
            }
        }
    }
}