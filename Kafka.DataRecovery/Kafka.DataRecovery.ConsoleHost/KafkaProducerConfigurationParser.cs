﻿using System.Collections.Generic;
using Kafka.DataRecovery.Worker;
using Kafka.DataRecovery.Worker.Configuration;

namespace Kafka.DataRecovery.ConsoleHost
{
    public static class KafkaProducerConfigurationParser 
    {
        public static Dictionary<string, object> Parse(KafkaProducerConfig config)
        {
            return new Dictionary<string, object>
            {
                { "batch.num.messages", config.BatchSize },
                { "bootstrap.servers", config.BrokersList },
                { "compression.codec", config.CompressionCodec },
                { "request.required.acks", config.RequiredAcks },
                { "queue.buffering.max.ms", config.LingerMilliseconds }
            };
        }
    }
}
