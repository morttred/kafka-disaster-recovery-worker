﻿using System.IO;
using System.Text;
using System.Threading.Tasks;
using Confluent.Kafka;
using Confluent.Kafka.Serialization;
using Kafka.DataRecovery.Worker.Configuration;
using Kafka.DataRecovery.Worker.Microservice;
using Kafka.DataRecovery.Worker.Repositories;
using Newtonsoft.Json.Linq;
using Serilog;
using Serilog.Core;
using StatsCollector;

namespace Kafka.DataRecovery.ConsoleHost
{
    public class Startup
    {
        public static void Main(string[] args)
        {
            MainAsync().Wait();
        }

        public static async Task MainAsync()
        {
            var workerConfig = CreateWorkerConfig();
            var worker = CreateWorker(workerConfig);

            worker.Initialize();
            await worker.Start();
        }

        private static DataRecoveryWorker CreateWorker(DataRecoveryWorkerConfig workerConfig)
        {
            var logger = CreateLogger();
            var producer = CreateKafkaProducer(workerConfig.KafkaProducerConfig);
            var statsCollector = CreateStatsCollector(workerConfig.StatsCollectorConfig);
            var amazonRepository = CreateAmazonRepository(workerConfig);

            return new DataRecoveryWorker(logger, statsCollector, producer, amazonRepository);
        }

        private static DataRecoveryWorkerConfig CreateWorkerConfig()
        {
            var workerConfigFilePath = "Configuration/config.json";
            var workerConfigFileContent = File.ReadAllText(workerConfigFilePath);
            var workerConfig = JObject.Parse(workerConfigFileContent).ToObject<DataRecoveryWorkerConfig>();

            return workerConfig;
        }

        private static AmazonRepository CreateAmazonRepository(DataRecoveryWorkerConfig workerConfig)
        {
            return new AmazonRepository(workerConfig.AmazonQueueConfig);
        }

        private static StatsCollector.StatsCollector CreateStatsCollector(StatsCollectorConfig config)
        {
            var statsCollector = new StatsCollector.StatsCollector();
            statsCollector.Configure(config);

            return statsCollector;
        }

        private static Logger CreateLogger()
        {
            return new LoggerConfiguration()
                .WriteTo.RollingFile("Log-{Date}.txt")
                .CreateLogger();
        }

        private static Producer<string, string> CreateKafkaProducer(KafkaProducerConfig config)
        {
            var producerConfiguration = KafkaProducerConfigurationParser.Parse(config);
            var producerKeySerializer = new StringSerializer(Encoding.UTF8);
            var producerValueSerializer = new StringSerializer(Encoding.UTF8);

            return new Producer<string, string>(producerConfiguration,
                producerKeySerializer,
                producerValueSerializer);
        }
    }
}
