# Kafka-Disaster-Recovery-Worker

Kafka disaster recovery worker POC.

It takes messages in bulks by a given date interval from Mongo or Amazon S3 and publishes them in the appropriate Kafka topics.